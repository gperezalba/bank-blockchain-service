import { ethers, Contract } from 'ethers';
import { constants } from '../environment/environment';

/***********BORRAR */

const CONTRACT_ADDRESS = "0xF172a37dAD95C7040a4Dbb1B933C6744f02f0E4a";
const CONTRACT_ABI = [{"constant":true,"inputs":[],"name":"x","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_x","type":"uint256"}],"name":"set","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"get","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_old","type":"uint256"},{"indexed":false,"name":"_new","type":"uint256"},{"indexed":true,"name":"sender","type":"address"},{"indexed":true,"name":"addr2","type":"address"}],"name":"Ev","type":"event"}];

function getWallet(): ethers.Wallet {
    let mnemonic = "shadow gain island same rely plate either isolate picture museum shield surface";
    return ethers.Wallet.fromMnemonic(mnemonic);
}

function getWalletSigner(): ethers.Wallet {
    return getWallet().connect(getProvider());
}

export async function simpleTx() {
    let tx = {
        to: "0x9b467D4477E51b40c497417B7af61bABec6e30c7",
        value: 1,
    }
    let receipt = await sendTransaction(getWallet(), tx);
    console.log(receipt)
}
/*********************** */

function getProvider(): ethers.providers.BaseProvider {
    //return new ethers.providers.JsonRpcProvider(constants.URL);
    return ethers.getDefaultProvider('ropsten');
}

export function createWalletFromEntropy(entropy: string): ethers.Wallet {
    return ethers.Wallet.createRandom(entropy);
}

//let mnemonic = "shadow gain island same rely plate either isolate picture museum shield surface";
export function createWalletFromMnemonic(mnemonic: string, path?: string): ethers.Wallet {
    //requerir isValidMnemonic == true?
    return ethers.Wallet.fromMnemonic(mnemonic, path);
}

export function isValidMnemonic(mnemonic: string): boolean {
    return ethers.utils.HDNode.isValidMnemonic(mnemonic);
}

//let privateKey = "0x0123456789012345678901234567890123456789012345678901234567890123";
export function createWalletFromPrivKey(privateKey: string): ethers.Wallet {
    return new ethers.Wallet(privateKey);
}

//let encryptedJson = {"version":3,"id":"148ea69d-1f23-48dc-a7d8-c316be23d0d3","address":"19e8d14be53249cd4aa1c8cb43425c0aeeb67fff","crypto":{"ciphertext":"8dc58c56ee705cfc661951397828bb2d464f1db9cad833cd1a6ef09eca7bb177","cipherparams":{"iv":"c6353dc63ef390991d7940e4efe0a8a7"},"cipher":"aes-128-ctr","kdf":"scrypt","kdfparams":{"dklen":32,"salt":"5a36acf0cc140298403787d1e1f957429ecd5a861064186df3f1a143c63dc651","n":131072,"r":8,"p":1},"mac":"3fcfc949627f018766a137e9235c984fa38997b4619aff8172a892c648373917"}};
//let pass = "contraprueba";
export async function createWalletFromEncryptedJson(encryptedJson: string, pasword: string): Promise<ethers.Wallet> {
    let json = JSON.stringify(encryptedJson);
    return await ethers.Wallet.fromEncryptedJson(json, pasword);
}

export async function encryptWallet(password: string, wallet: ethers.Wallet): Promise<string> {
    return await wallet.encrypt(password);
}

export async function sendTransaction(wallet: ethers.Wallet, transaction: any): Promise<ethers.providers.TransactionReceipt> {
    let populatedTransaction = await createTransaction(wallet, transaction);
    let signedTransaction = await signTransaction(wallet, populatedTransaction);
    let response = await sendSignedTransaction(signedTransaction);
    let receipt = await response.wait();

    return receipt;
}

/*export async function createTransaction(wallet: Wallet, gasLimit: number, gasPrice: string, to: string, data: string): Promise<ethers.providers.TransactionRequest> {
    const customHttpProvider = new ethers.providers.JsonRpcProvider(constants.URL);
    let walletInstance = new ethers.Wallet(wallet.privateKey, customHttpProvider);
    let nonce = await walletInstance.getTransactionCount();

    return {
        nonce: ethers.utils.bigNumberify(nonce), 
        gasLimit: ethers.utils.bigNumberify(gasLimit), 
        gasPrice: ethers.utils.bigNumberify(gasPrice), 
        to, 
        value: ethers.utils.bigNumberify(0), 
        data: data, 
        chainId: constants.CHAIN_ID
    };
}*/

export async function createTransaction(wallet: ethers.Wallet, transaction: any): Promise<ethers.providers.TransactionRequest> {
    const customHttpProvider = getProvider();
    let populatedTransaction = await ethers.utils.populateTransaction(transaction, customHttpProvider, wallet.address);

    if (populatedTransaction.gasPrice.lt(ethers.utils.bigNumberify(constants.MIN_GAS_PRICE))) {
        populatedTransaction.gasPrice = ethers.utils.bigNumberify(constants.MIN_GAS_PRICE);
    }

    return populatedTransaction;
}

export async function signTransaction(wallet: ethers.Wallet, transaction: ethers.providers.TransactionRequest): Promise<string> {
    let walletInstance = new ethers.Wallet(wallet.privateKey);
    return await walletInstance.sign(transaction);
}

export async function sendSignedTransaction(signedTransaction: string): Promise<ethers.providers.TransactionResponse> {
    const customHttpProvider = getProvider();
    return await customHttpProvider.sendTransaction(signedTransaction);
}

export function etherStringToWeiBN(ether: string): ethers.utils.BigNumber {
    return ethers.utils.parseEther(ether);
}

export function etherStringToWeiString(ether: string): string {
    return ethers.utils.parseEther(ether).toString();
}

export function weiStringToEtherBN(wei: string): ethers.utils.BigNumber {
    return ethers.utils.bigNumberify(ethers.utils.formatEther(wei));
}

export function weiToEtherBN(weiBN: ethers.utils.BigNumber): ethers.utils.BigNumber {
    return ethers.utils.bigNumberify(ethers.utils.formatEther(weiBN));
}

export function weiToEther(wei: string): string {
    return ethers.utils.formatEther(wei);
}

export function weiBNToEtherString(bn: ethers.utils.BigNumber): string {
    return ethers.utils.formatEther(bn.toString());
}

export function BNToString(bn: ethers.utils.BigNumber): string {
    return bn.toString();
}

export function StringToBN(str: string): ethers.utils.BigNumber {
    return ethers.utils.bigNumberify(str);
}

export function commify(ether: string): string {
    return ethers.utils.commify(ether);
}

export function commifyBN(bn: ethers.utils.BigNumber): string {
    return ethers.utils.commify(bn.toString());
}

export function getContractCaller(address: string, abi: any | ethers.utils.Interface): ethers.Contract {
    return new ethers.Contract(address, abi, getProvider());
}

export function getContractSigner(address: string, abi: any | ethers.utils.Interface, wallet: ethers.Wallet): ethers.Contract {
    //requerir wallet.provider != null?
    return getContractCaller(address, abi).connect(wallet);
}

export async function getPastEvents(contract: ethers.Contract, filter: ethers.EventFilter, fromBlock: number, toBlock: number | string) {
    let provider = getProvider();
    let logsFilter = {
        address: contract.address,
        fromBlock: fromBlock,
        toBlock: toBlock,
        topics: filter.topics
    }
    let logs = await provider.getLogs(logsFilter);

    return logs;
}

export async function decodeEvent(contract: ethers.Contract, log: {topics: string [], data: string}): Promise<any> {
    return await contract.interface.parseLog(log).values;
}

export async function querySubgraph(query: string): Promise<any> {
    try {
        let response = await fetch('http://<IP>:<PORT>/subgraphs/name/repository_name/subgraph_name', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                //query: "{ transactions(orderBy: timestamp, orderDirection: desc) { id from to amount concept timestamp } }"
                query: query
            }),
            //si no es en body la query va en el campo data (-d)
        });
        let json = await response.json();
        console.log(json)
        return json;
    } catch (error) {
        console.error(error);
    }
}

/*
QUERY RULES

1. With ID
{
  token(id: "1") {
    id
    owner
  }
}

2. Without ID
{
  tokens {
    id
    owner
  }
}

3. Sorting
{
  tokens(orderBy: price, orderDirection: asc | desc) {
    id
    owner
  }
}

4. Pagination
{
  tokens(first:1000, skip:1000) { //[1000-1999]
    id
    owner
  }
}

5. Filtering
{
  challenges(where: { outcome: "failed" }) {
    challenger
  }
}

6. Filtering with suffixes
{
  applications(where: { deposit_gt: "10000000000" }) {
    id
    deposit
  }
}

_not
_gt
_lt
_gte
_lte
_in
_not_in
_contains
_not_contains
_starts_with
_ends_with
_not_starts_with
_not_ends_with

7. Query a certain past block
{
  challenges(block: { number: 8000000 }) {
    challenger
    outcome
  }
}

{
  challenges(block: { hash: "0x5a0b54d5dc17e0aadc383d2db43b0a0d3e029c4c" }) {
    challenger
    outcome
  }
}

*/

/*

BANK QUERIES

1. SALDOS EN TODOS LOS TOKENS QUE POSEE EL WALLET
{
    wallet (where: {id: "wallet_address"}) {
        balances {
            token {
                tokenSymbol
            }
            balance
        }
    }
}

2. ÚLTIMOS 20 MOVIMIENTOS DEL WALLET
{
    transactions 
    (
        where: {currency: "token_address", from: "wallet_address"},
        first: 20,
        skip: 20,
        orderBy: timestamp,
        orderDirection: desc
    ) 
    {
        from
        to
        amount
        timestamp
        bankTransaction {
            concept
        }
    }
}

*/

/**********************IDENTITY FUNCTIONS */

export async function forward(signer: ethers.Wallet, identity: string, destination: string, data: string): Promise<ethers.providers.TransactionResponse>  {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.forward(destination, data);
    return await response.wait();
}

export async function setState(signer: ethers.Wallet, identity: string, newState: ethers.utils.BigNumber): Promise<ethers.providers.TransactionResponse> {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.setState(newState);
    return await response.wait();
}    

export async function setOwner(signer: ethers.Wallet, identity: string, newOwner: ethers.utils.BigNumber): Promise<ethers.providers.TransactionResponse> {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.setOwner(newOwner);
    return await response.wait();
}    

export async function setRecovery(signer: ethers.Wallet, identity: string, newRecovery: ethers.utils.BigNumber): Promise<ethers.providers.TransactionResponse> {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.setRecovery(newRecovery);
    return await response.wait();
}    

/**********************WALLET FUNCTIONS */

export async function transfer(signer: ethers.Wallet, identity: string, wallet: string, tx: {tokenAddress: string, to: string, value: ethers.utils.BigNumber, data: string}): Promise<ethers.providers.TransactionResponse> {
    //let wallet = getWalletByIdentity(identity); DEBO TENER MAPEO DE ESTO AQUÍ? O DONDE?
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transfer.encode([tx.tokenAddress, tx.to, tx.value, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferSending(signer: ethers.Wallet, identity: string, wallet: string, tx: {tokenAddress: string, to: string, value: ethers.utils.BigNumber, data: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferSending.encode([tx.tokenAddress, tx.to, tx.value, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferDomain(signer: ethers.Wallet, identity: string, wallet: string, tx: {tokenAddress: string, name: string, value: ethers.utils.BigNumber, data: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferDomain.encode([tx.tokenAddress, tx.name, tx.value, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeReceiving(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, transferingAmount: ethers.utils.BigNumber, to: string, data: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeReceiving.encode([tx.sendingToken, tx.transferingToken, tx.transferingAmount, tx.to, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeSending(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, sendingAmount: ethers.utils.BigNumber, to: string, data: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeSending.encode([tx.sendingToken, tx.transferingToken, tx.sendingAmount, tx.to, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferDomainSending(signer: ethers.Wallet, identity: string, wallet: string, tx: {tokenAddress: string, name: string, data: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferDomainSending.encode([tx.tokenAddress, tx.name, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeDomainReceiving(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, sendingAmount: ethers.utils.BigNumber, name: string, data: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeDomainReceiving.encode([tx.sendingToken, tx.transferingToken, tx.sendingAmount, tx.name, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeDomainSending(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, sendingAmount: ethers.utils.BigNumber, name: string, data: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeDomainSending.encode([tx.sendingToken, tx.transferingToken, tx.sendingAmount, tx.name, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function exchange(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, receivingToken: string, amount: ethers.utils.BigNumber}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.exchange.encode([tx.sendingToken, tx.receivingToken, tx.amount]);
    return await forward(signer, identity, wallet, data);
}

export async function limitValue(signer: ethers.Wallet, identity: string, wallet: string, tx: {tokenAddress: string, limit: ethers.utils.BigNumber}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.limitValue.encode([tx.tokenAddress, tx.limit]);
    return await forward(signer, identity, wallet, data);
}

export async function limitTo(signer: ethers.Wallet, identity: string, wallet: string, tx: {receiver: string}): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.limitTo.encode([tx.receiver]);
    return await forward(signer, identity, wallet, data);
}

export async function unlimitValue(signer: ethers.Wallet, identity: string, wallet: string): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.unlimitValue.encode([]);
    return await forward(signer, identity, wallet, data);
}

export async function unlimitTo(signer: ethers.Wallet, identity: string, wallet: string): Promise<ethers.providers.TransactionResponse> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.unlimitValue.encode([]);
    return await forward(signer, identity, wallet, data);
}

//CALLS

export async function getTransferExchangeInfoSending(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getTransferExchangeInfoSending();
    return response;
}

export async function getTransferExchangeInfoReceiving(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getTransferExchangeInfoReceiving();
    return response;
}

export async function getExchangeInfoSending(wallet: string): Promise<[ethers.utils.BigNumber]> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getExchangeInfoSending();
    return response;
}

export async function getExchangeInfoReceiving(wallet: string): Promise<[ethers.utils.BigNumber]> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getExchangeInfoReceiving();
    return response;
}

export async function getValueToSpend(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getValueToSpend();
    return response;
}

export async function getSpendToValue(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getSpendToValue();
    return response;
}

/**********************PANTALLA CREAR CUENTAS */

export function getDataHash(data: {name: string, country: string, nif: string}): string {
    //SOLUCIÓN TEMPORAL
    //REPASAR SI USAR EL SOLIDITYPACK
    //REPASAR SI USAR KECCA256 O SOLIDITYKECCKA256 O SHA256
    let packed = ethers.utils.solidityPack(["string", "string", "string"], [data.name, data.country, data.nif]);
    return ethers.utils.keccak256(packed);
}

/*export async function isNameAvailable(name: string): Promise<boolean> {
    let nameServiceContract = getContractCaller(NAME_SERVICE_ADDRESS, constants.NAME_SERVICE_ABI);
    return await nameServiceContract.nameIsAvailable(name);
}*/

export function createWalletPair(
        entropy: string
    )
    : 
    {
        owner: string;
        recovery: string;
        mnemonic: string;
        privateKey: string;
    } 
{
    let wallet = createWalletFromEntropy(entropy);
    let owner = createWalletFromMnemonic(wallet.mnemonic, constants.PATH_0);
    let recovery = createWalletFromMnemonic(wallet.mnemonic, constants.PATH_1);

    return {
        owner: owner.address,
        recovery: recovery.address,
        mnemonic: wallet.mnemonic,
        privateKey: wallet.privateKey
    }
}

export async function checkIdentity()/*: Promise<boolean>*/ {
    //TODO
    //comprobar vs blockchain
    //vs identity el name y el wallet
    //vs registry el hash 
}


/**********************EJEMPLOS */

export async function get(): Promise<any> {
    let contract = getContractCaller(CONTRACT_ADDRESS, CONTRACT_ABI);
    return await contract.get();
}

export async function set(newValue: string, wallet: ethers.Wallet, overrides?: {nonce?: ethers.utils.BigNumber, gasLimit?: ethers.utils.BigNumber, gasPrice?: ethers.utils.BigNumber, value?: ethers.utils.BigNumber}): Promise<any> {
    let contract = getContractSigner(CONTRACT_ADDRESS, CONTRACT_ABI, wallet);
    let response = await contract.set(newValue, overrides);
    return await response.wait();
}

/**********************BORRAR */
export async function test() {
    let c = getContractCaller(CONTRACT_ADDRESS, CONTRACT_ABI);
    let x = await c.get();
    console.log(x.toString())
}

export async function test2() {
    let wallet = getWalletSigner();
    let c = getContractSigner(CONTRACT_ADDRESS, CONTRACT_ABI, wallet);
    let x = await c.get();
    console.log(x.toString())
    let resp = await c.set("2");
    console.log(resp);
    let receipt = await resp.wait();
    console.log(receipt);
    let x2 = await c.get();
    console.log(x2.toString())
}

export async function test3() {
    let c = getContractCaller(CONTRACT_ADDRESS, CONTRACT_ABI);
    let est = await c.estimate.set("23");
    console.log(est.toString())
}

export async function test4() {
    let c = getContractCaller(CONTRACT_ADDRESS, CONTRACT_ABI);
    //console.log(c)
    let filter = c.filters.Ev(null, null, null, "0xF172a37dAD95C7040a4Dbb1B933C6744f02f0E4a");
    //console.log(filter)
    //let Ev = c.interface.events.Ev.topic;
    let provider = getProvider();
    let logsFilter = {
        address: c.address,
        fromBlock: 0,
        toBlock: "latest",
        topics: c.filters.Ev(null, null, null, "0x9b467D4477E51b40c497417B7af61bABec6e30c7").topics
    }
    let logs = await provider.getLogs(logsFilter);
    console.log(logs)
    let ev = await c.interface.parseLog({topics: logs[0].topics, data: logs[0].data})
    let ev2 = c.interface.events.Ev.decode(logs[0].data, logs[0].topics)
    console.log(ev.values)
    console.log(ev2)

}

export async function test5() {
    let c = getContractCaller(CONTRACT_ADDRESS, CONTRACT_ABI);
    let filter = c.filters.Ev(null, null, null, "0xF172a37dAD95C7040a4Dbb1B933C6744f02f0E4a");
    let filter2 = c.filters.Ev(null, null, null, "0x9b467D4477E51b40c497417B7af61bABec6e30c7");
    console.log(await getPastEvents(c, filter, 0, "latest"));
}

export async function test6() {
    let wallet = getWalletSigner();
    let c = getContractSigner(CONTRACT_ADDRESS, CONTRACT_ABI, wallet);
    let data = c.interface.functions.set.encode(["3"])
    
    console.log(data)
}

export async function test7() {

    
    /*fetch('https://mywebsite.com/endpoint/', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            query: "{ transactions(orderBy: timestamp, orderDirection: desc) { id from to amount concept timestamp } }"
        }),
        //si no es en body la query va en el campo data (-d)
    });
    */

    try {
        let response = await fetch('https://reqres.in/api/users', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "name": "morpheus",
                "job": "leader"
            }),
        });
        let json = await response.json();
        console.log(json)
    } catch (error) {
        console.error(error);
    }
}