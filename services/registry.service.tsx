import { ethers, Contract, Wallet } from 'ethers';
import { constants } from '../environment/environment';
import { WalletPair } from '../classes/walletPair';
import { getContractCaller, getContractSigner, createWalletFromPrivKey, createWalletFromEntropy, createWalletFromMnemonic, encryptWallet, decodeEvent } from './blockchain.service';
import { WalletPairSafe } from '../classes/walletPairSafe';

/*************PANTALLA REGISTRO NUEVO USUARIO */
/*
1. Formulario con toda la info que tenga que proporcionar para DD
2. Creación de wallets (owner y recovery). 
    a. Función "createWalletPair".
    b. A la función hay que pasarle algo aleatorio como fuente de entropía (en string)
    c. Sería bueno (si es fácil) que el usuario pueda influir en esa entropía (dibujando algo 
        en pantalla, con el sensor de movimiento del móvil...) (OPCIONAL)
    d. El usuario tendrá que indicar una contraseña para cifrar su wallet en su dispositivo móvil
    e. Al usuario se le mostrará en pantalla su wallet Y SU MNEMÓNICO (12 palabritas)
    f. Hay que asegurarse que las apunta y guarda en lugar seguro...pedir en la siguiente pantallita
        que las escriba en orden (como Metamask). Hay un diccionario FINITO de las palabras que se usan
        si lo necesitas te lo busco (para que vaya autocompletando cuando escriba o algo).
    g. Se debe eliminar por completo del dispositivo el mnemónico. En el dispositivo sólo debe guardarse
        el encryptedWallet que devuelve "createWalletPair" que se (des)encripta con la contraseña del user
3. Por último tendrá que indicar cual quiere que sea su nickname (puede ir en el formulario si quieres)
4. Hay que checkear contra blockchain si el nombre está disponible con "isNameAvailable(string)"
5. Se envía la solicitud de registro que contendrá:
    a. Formulario
    b. Wallets owner y recovery (SÓLO CLAVE PÚBLICA)
    c. Nickname
6. Se realiza todo el proceso de DD
7. Aceptar el registro conllevará un proceso del backEnd que realice una llamada a "deployIdentity" con:
    a. Owner
    b. Recovery
    c. Nickname
    d. dataHash. Esto se obtiene haciendo una llamada a "getDataHash" pasándole la info que acordemos
        (nombre, apellidos, país, nif...). En blockchain se registrará este hash contra la dirección
        de su contrato de identidad digital, en blockchain no tendremos info en texto plano.
    e. Notar que antes de llamar a "deployIdentity" se llamará a "checkDataHash" para asegurarnos de 
        que ese dataHash no existe previamente.
*/
/******************************************** */

export async function createWalletPair(
    entropy: string,
    password: string
)
    : Promise<WalletPair>
{
    const wallet = createWalletFromEntropy(entropy);
    const owner = createWalletFromMnemonic(wallet.mnemonic, constants.PATH_0);
    const onlyOwner = createWalletFromPrivKey(owner.privateKey);
    const scryptOptions = { scrypt: { N: 64 } };
    const encryptedOwner = await onlyOwner.encrypt(password, scryptOptions);
    //PENSAR SI UTILIZAR OTRO TREE PARA EL RECOVERY
    const recovery = createWalletFromMnemonic(wallet.mnemonic, constants.PATH_1);

    return new WalletPair(owner.address, recovery.address, wallet.mnemonic, encryptedOwner);
}

export async function createWalletPairSafe(
    entropy: string,
    password: string
)
    : Promise<WalletPair>
{
    const wallet = createWalletFromEntropy(entropy);
    const owner = createWalletFromMnemonic(wallet.mnemonic, constants.PATH_0);
    const onlyOwner = createWalletFromPrivKey(owner.privateKey);
    const scryptOptions = { scrypt: { N: 64 } };
    const encryptedWallet = await wallet.encrypt(password, scryptOptions);
    const encryptedOwner = await onlyOwner.encrypt(password, scryptOptions);

    //PENSAR SI UTILIZAR OTRO TREE PARA EL RECOVERY
    const recovery = createWalletFromMnemonic(wallet.mnemonic, constants.PATH_1);

    return new WalletPairSafe(owner.address, recovery.address, wallet.mnemonic, encryptedOwner, encryptedWallet);
}

export async function isNameAvailable(name: string): Promise<boolean> {
    let controllerContract = getContractCaller(constants.CONTROLLER_ADDRESS, constants.CONTROLLER_ABI);
    let registryAddress = await controllerContract.addresses("6");
    let nameServiceContract = getContractCaller(registryAddress, constants.NAME_SERVICE_ABI);
    return await nameServiceContract.nameIsAvailable(ethers.utils.id(name));
}

/********** BACKEND */

export async function deployIdentity(owner: string, recovery: string, dataHash: string, nickname: string) {
    let controllerContract = getContractCaller(constants.CONTROLLER_ADDRESS, constants.CONTROLLER_ABI);
    let identityFactoryAddress = await controllerContract.addresses("2");
    //SIGNER FOR TESTNET
    let signer = createWalletFromPrivKey(constants.TESTNET_OWNER);
    let identityFactory = getContractSigner(identityFactoryAddress, constants.IDENTITY_FACTORY_ABI, signer);
    let response =  await identityFactory.deployIdentity(owner, recovery, dataHash, nickname, constants.OVERRIDES_BACKEND);
    let receipt = await response.wait();
    
    let event;
			for (let i = 0; i < receipt.logs.length; i++) {
				if (receipt.logs[i].address.toLowerCase() == identityFactoryAddress.toLowerCase()) {
					let topics = receipt.logs[i].topics;
					let data = receipt.logs[i].data;
					let _log = {topics, data};
					event = await decodeEvent(identityFactory, _log)
				}
			}

			return event.wallet;
}

export function getDataHash(name: string, country: string, nif: string): string {
    //SOLUCIÓN TEMPORAL
    //REPASAR SI USAR EL SOLIDITYPACK
    //REPASAR SI USAR KECCA256 O SOLIDITYKECCKA256 O SHA256
    //CONCRETAR QUÉ INFO USAR PARA EL HASH
    let packed = ethers.utils.solidityPack(["string", "string", "string"], [name, country, nif]);
    return ethers.utils.keccak256(packed);
}

export async function checkDataHash(dataHash: string): Promise<boolean> {
    let controllerContract = getContractCaller(constants.CONTROLLER_ADDRESS, constants.CONTROLLER_ABI);
    let registryAddress = await controllerContract.addresses("1");
    let registryContract = getContractCaller(registryAddress, constants.REGISTRY_ABI);
    return await registryContract.isHashAvailable(dataHash);
}

export async function setNewIdentityDD(identity: string, dataHashDD: string): Promise<ethers.providers.TransactionReceipt> {
    let controllerContract = getContractCaller(constants.CONTROLLER_ADDRESS, constants.CONTROLLER_ABI);
    let registryAddress = await controllerContract.addresses("1");
    //SIGNER FOR TESTNET
    let signer = createWalletFromPrivKey(constants.TESTNET_OWNER);
    let registryContract = getContractSigner(registryAddress, constants.REGISTRY_ABI, signer);
    let response = await registryContract.setNewIdentityDD(identity, dataHashDD, constants.OVERRIDES_BACKEND);
    return await response.wait();
}

export async function setNewHashKYC(identity: string, dataHashDD: string): Promise<ethers.providers.TransactionReceipt> {
    let controllerContract = getContractCaller(constants.CONTROLLER_ADDRESS, constants.CONTROLLER_ABI);
    let registryAddress = await controllerContract.addresses("19");
    //SIGNER FOR TESTNET
    let signer = createWalletFromPrivKey(constants.TESTNET_OWNER);
    let registryContract = getContractSigner(registryAddress, constants.REGISTRY_KYC_ABI, signer);
    let response = await registryContract.setNewHash(identity, dataHashDD, constants.OVERRIDES_BACKEND);
    return await response.wait();
}