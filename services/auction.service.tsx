import { ethers } from "ethers";
import { AuctionParams } from "../classes/auctionParams";
import { constants } from "../environment/environment";
import { forward, getContractSigner, getControllerAddress } from "./blockchain.service";

// WHEN AUCTION TOKEN IS ERC223 OR COLLECTABLE
export async function deployAuction(
    identity: string, 
    wallet: string, 
    signer: ethers.Wallet,
    auction: AuctionParams
) {
    let auctionFactoryAddress = await getControllerAddress("20");
    let auctionFactory = getContractSigner(
        auctionFactoryAddress, 
        constants.AUCTION_FACTORY_ABI, 
        signer
    );
    let deployAuctionData = auctionFactory.interface.functions.deployAuction.encode([
        auction.auditor,
        auction.tokens,
        auction.auctionAmountOrId,
        auction.auctionTokenId,
        auction.settings
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        auction.tokens[0],
        auction.auctionAmountOrId,
        auctionFactoryAddress,
        deployAuctionData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    return receipt;
}

// WHEN AUCTION TOKEN IS PACKABLE
export async function deployAuctionPackable(
    identity: string, 
    wallet: string, 
    signer: ethers.Wallet,
    auction: AuctionParams
) {
    let auctionFactoryAddress = await getControllerAddress("20");
    let auctionFactory = getContractSigner(
        auctionFactoryAddress, 
        constants.AUCTION_FACTORY_ABI, 
        signer
    );
    let deployAuctionData = auctionFactory.interface.functions.deployAuction.encode([
        auction.auditor,
        auction.tokens,
        auction.auctionAmountOrId,
        auction.auctionTokenId,
        auction.settings
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValuePNFT.encode([
        auction.tokens[0],
        auction.auctionTokenId,
        auction.auctionAmountOrId,
        auctionFactoryAddress,
        deployAuctionData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    return receipt;
}

export async function bid(
    identity: string, 
    wallet: string, 
    signer: ethers.Wallet,
    auctionAddress: string,
    bidToken: string,
    bid: ethers.utils.BigNumber,
    minValue: ethers.utils.BigNumber
) {
    let auctionFactory = getContractSigner(
        auctionAddress, 
        constants.AUCTION_ABI, 
        signer
    );
    let bidData = auctionFactory.interface.functions.bid.encode([
        bid
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        bidToken,
        minValue,
        auctionAddress,
        bidData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    return receipt;
}

export async function updateBid(
    identity: string, 
    wallet: string, 
    signer: ethers.Wallet,
    auctionAddress: string,
    bid: ethers.utils.BigNumber
) {
    let auctionFactory = getContractSigner(
        auctionAddress, 
        constants.AUCTION_ABI, 
        signer
    );
    let bidData = auctionFactory.interface.functions.updateBid.encode([
        bid
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        auctionAddress,
        bidData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    return receipt;
}

export async function cancelBid(
    identity: string, 
    wallet: string, 
    signer: ethers.Wallet,
    auctionAddress: string
) {
    let auctionFactory = getContractSigner(
        auctionAddress, 
        constants.AUCTION_ABI, 
        signer
    );
    let bidData = auctionFactory.interface.functions.cancelBid.encode([]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        auctionAddress,
        bidData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    return receipt;
}

export async function payDeal(
    identity: string, 
    wallet: string, 
    signer: ethers.Wallet,
    auctionAddress: string,
    bidToken: string,
    amountLeft: ethers.utils.BigNumber
) {
    let auctionFactory = getContractSigner(
        auctionAddress, 
        constants.AUCTION_ABI, 
        signer
    );
    let bidData = auctionFactory.interface.functions.bid.encode([]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        bidToken,
        amountLeft,
        auctionAddress,
        bidData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    return receipt;
}