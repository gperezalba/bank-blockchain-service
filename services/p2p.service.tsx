import { getContractSigner, forward, getControllerAddress, querySubgraph, createWalletFromPrivKey, decodeEvent } from "./blockchain.service";
import { constants } from "../environment/environment";
import { Wallet, ethers } from "ethers";
import { P2POffer } from "../classes/p2pOffer";
import { TransactionReceipt } from "ethers/providers";
import { P2POfferCommodity } from "../classes/p2pOfferCommodity";
import { P2POfferPackable } from "../classes/p2pOfferPackable";

/********************** P2P TOKEN FIAT */

export async function offer(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerParams: P2POffer
)
    : Promise<any[]>
{
    let p2pAddress = await getControllerAddress("9");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    let p2pData = p2pContract.interface.functions.offer.encode([
        offerParams.tokens, 
        offerParams.amounts, 
        offerParams.settings, 
        offerParams.limits,
        offerParams.auditor,
        offerParams.description,
        offerParams.metadata
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        offerParams.tokens[0],
        offerParams.amounts[0],
        p2pAddress,
        p2pData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    let event;
    if (receipt.logs != undefined) {
        for (let i = 0; i < receipt.logs.length; i++) {
            if (receipt.logs[i].address.toLowerCase() == p2pAddress.toLowerCase()) {
                let topics = receipt.logs[i].topics;
                let data = receipt.logs[i].data;
                let _log = {topics, data};
                event = await decodeEvent(p2pContract, _log)
            }
        }
    }

    return [event.offerId, receipt.transactionHash];
}

export async function cancelOffer(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string
) {
    let p2pAddress = await getControllerAddress("9");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    let p2pData = p2pContract.interface.functions.cancelOffer.encode([
        offerId
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function updateBuyAmount(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber
) {
    let p2pAddress = await getControllerAddress("9");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    let p2pData = p2pContract.interface.functions.updateBuyAmount.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

//Cuando el comprador lo que envía es FIAT
export async function deal(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber
) {
    let p2pAddress = await getControllerAddress("9");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    let p2pData = p2pContract.interface.functions.deal.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    let receipt = await forward(signer, identity, wallet, walletData);

    let event;
    if (receipt.logs != undefined) {
        for (let i = 0; i < receipt.logs.length; i++) {
            if (receipt.logs[i].address.toLowerCase() == p2pAddress.toLowerCase()) {
                if (receipt.logs[i].topics[0] == "0x75c48d2c41d94e0ba2f763f7aa64a7cae7a2802b6e471cb4ccff923c99e03977") {
                    let topics = receipt.logs[i].topics;
                    let data = receipt.logs[i].data;
                    let _log = {topics, data};
                    event = await decodeEvent(p2pContract, _log)
                }
            }
        }
    }

    return [event.dealId, receipt.transactionHash];
}

//Cuando el comprador lo que envía es TOKEN
export async function dealToken(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber,
    buyToken: string //SI LA APP NO LO TIENE FÁCIL LO PILLO DEL SUBGRAPH
) {
    let p2pAddress = await getControllerAddress("9");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    let p2pData = p2pContract.interface.functions.deal.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        buyToken,
        buyAmount,
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function voteDeal(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    dealId: string,
    vote: "1" | "2" //1- para confirmar pacto y 2- para revocarlo
) {
    let p2pAddress = await getControllerAddress("9");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    let p2pData = p2pContract.interface.functions.voteDeal.encode([
        dealId,
        vote
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

//Si la otra parte no vota nada al pacto, esto emite un evento para que le aparezca al auditor
//que debe actuar. Pasa lo mismo (automáticamente) si las partes del pacto votan diferente.
export async function requestAuditor(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    dealId: string
) {
    let p2pAddress = await getControllerAddress("9");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    let p2pData = p2pContract.interface.functions.requestAuditor.encode([
        dealId
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}


/********************** P2P TOKEN COMMODITY */

//SI TE PETA ESTA LLAMADA DIMELO PROBABLEMENTE HAYA QUE HACER EL ARRAY METADATA CON STRING EN VEZ DE CON NUMBERS
export async function offerCommodity(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerParams: P2POfferCommodity
)
    : Promise<TransactionReceipt>
{
    let p2pAddress = await getControllerAddress("10");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_NFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.offer.encode([
        offerParams.sellToken, 
        offerParams.sellId, 
        offerParams.buyToken,
        offerParams.buyAmount,
        offerParams.isBuyFiat,
        offerParams.minReputation,
        offerParams.auditor,
        offerParams.description,
        offerParams.metadata
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        offerParams.sellToken,
        offerParams.sellId,
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function cancelOfferCommodity(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string
) {
    let p2pAddress = await getControllerAddress("10");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_NFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.cancelOffer.encode([
        offerId
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function updateBuyAmountCommodity(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber
) {
    let p2pAddress = await getControllerAddress("10");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_NFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.updateBuyAmount.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

//Cuando el comprador lo que envía es TOKEN
export async function dealTokenCommodity(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber,
    buyToken: string //SI LA APP NO LO TIENE FÁCIL LO PILLO DEL SUBGRAPH
) {
    let p2pAddress = await getControllerAddress("10");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_NFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.deal.encode([
        offerId
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        buyToken,
        buyAmount,
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

//Cuando el comprador lo que envía es FIAT
export async function dealFiatCommodity(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string
) {
    let p2pAddress = await getControllerAddress("10");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_NFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.deal.encode([
        offerId
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function updateReputation(
    user: string,
    reputation: string
) {
    let p2pAddress = await getControllerAddress("9");
    let signer = createWalletFromPrivKey(constants.TESTNET_OWNER);
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_ABI, signer);
    
    let response =  await p2pContract.updateReputation(user, reputation, constants.OVERRIDES_BACKEND);
    return await response.wait();
}

/******************* P2P PACKABLE */

export async function offerPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerParams: P2POfferPackable
)
    : Promise<TransactionReceipt>
{
    let p2pAddress = await getControllerAddress("13");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.offer.encode([
        offerParams.tokens, 
        offerParams.tokenId, 
        offerParams.amounts,
        offerParams.settings,
        offerParams.limits,
        offerParams.auditor,
        offerParams.description,
        offerParams.metadata,
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValuePNFT.encode([
        offerParams.tokens[0],
        offerParams.tokenId,
        offerParams.amounts[0],
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function offerTokenRequestPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerParams: P2POfferPackable
)
    : Promise<TransactionReceipt>
{
    let p2pAddress = await getControllerAddress("17");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.offer.encode([
        offerParams.tokens, 
        offerParams.tokenId, 
        offerParams.amounts,
        offerParams.settings,
        offerParams.limits,
        offerParams.auditor,
        offerParams.description,
        offerParams.metadata,
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        offerParams.tokens[0],
        offerParams.amounts[0],
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function offerFiatRequestPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerParams: P2POfferPackable
)
    : Promise<TransactionReceipt>
{
    let p2pAddress = await getControllerAddress("17");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.offer.encode([
        offerParams.tokens, 
        offerParams.tokenId, 
        offerParams.amounts,
        offerParams.settings,
        offerParams.limits,
        offerParams.auditor,
        offerParams.description,
        offerParams.metadata,
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function updateBuyAmountPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber
) {
    let p2pAddress = await getControllerAddress("13");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.updateBuyAmount.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

export async function cancelOfferPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string
) {
    let p2pAddress = await getControllerAddress("13");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.cancelOffer.encode([
        offerId
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

//PARA OFERTAS DE PACKABLE VS TOKEN
export async function dealTokenPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber,
    buyToken: string //SI LA APP NO LO TIENE FÁCIL LO PILLO DEL SUBGRAPH
) {
    let p2pAddress = await getControllerAddress("13");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.deal.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValue.encode([
        buyToken,
        buyAmount,
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

//PARA OFERTAS DE PACKABLE VS FIAT
export async function dealFiatPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber,
    buyToken: string //SI LA APP NO LO TIENE FÁCIL LO PILLO DEL SUBGRAPH
) {
    let p2pAddress = await getControllerAddress("13");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.deal.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forward.encode([
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

//PARA OFERTAS TOKEN O FIAT VS PACKABLE
export async function dealPackable(
    identity: string, 
    wallet: string, 
    signer: Wallet,
    offerId: string,
    buyAmount: ethers.utils.BigNumber,
    buyToken: string, //SI LA APP NO LO TIENE FÁCIL LO PILLO DEL SUBGRAPH
    tokenId: string
) {
    let p2pAddress = await getControllerAddress("17");
    let p2pContract = getContractSigner(p2pAddress, constants.P2P_PNFT_ABI, signer);
    let p2pData = p2pContract.interface.functions.deal.encode([
        offerId,
        buyAmount
    ]);

    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let walletData = walletContract.interface.functions.forwardValuePNFT.encode([
        buyToken,
        tokenId,
        buyAmount,
        p2pAddress,
        p2pData
    ]);

    return await forward(signer, identity, wallet, walletData);
}

/******************* SUBGRAPH QUERIES */

export async function getOffersByTokens(
    sellToken: string, 
    buyToken: string, 
    isOpen: boolean, 
    isBuyFiat: boolean,
    payMethods: string[],
    country: string[],
    orderDirection: "asc" | "desc",
    orderBy: "timestamp" | "price",
    first: number,
    skip: number
) {
    let query = '{ offers(orderBy: '+ orderBy +', orderDirection: '+ orderDirection +', first: '+ first +', skip: '+ skip +', where: { payMethod_contains: ' + payMethods + ', country_contains: ' + country +', isOpen: ' + isOpen + ', isBuyFiat: ' + isBuyFiat + ', sellToken: "' + sellToken + '", buyToken: "' + buyToken + '" }) { price, timestamp, id owner { id goodReputation badReputation } sellToken { id tokenSymbol } sellAmount buyAmount buyToken { id tokenSymbol } isPartial isBuyFiat auditor description isOpen country subcountry subsubcountry payMethod } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

export async function getOffersByOwner(
    owner: string,
    isOpen: boolean,
    orderDirection: "asc" | "desc",
    orderBy: "timestamp" | "price",
    first: number,
    skip: number
) {
    let query = '{ offers(orderBy: '+ orderBy +', orderDirection: '+ orderDirection +', first: '+ first +', skip: '+ skip +', where: { owner: "' + owner + '", isOpen: '+ isOpen +' }) { price, timestamp, id owner { id goodReputation badReputation } sellToken { id tokenSymbol } sellAmount buyAmount buyToken { id tokenSymbol } isPartial isBuyFiat auditor description isOpen } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

export async function getOffersByTokensByOwner(
    owner: string,
    sellToken: string, 
    buyToken: string, 
    isOpen: boolean, 
    isBuyFiat: boolean,
    orderDirection: "asc" | "desc",
    orderBy: "timestamp" | "price",
    first: number,
    skip: number
) {
    let query = '{ offers(orderBy: '+ orderBy +', orderDirection: '+ orderDirection +', first: '+ first +', skip: '+ skip +', where: { owner: "' + owner + '", isOpen: '+ isOpen +', isBuyFiat: '+ isBuyFiat +', sellToken: "'+ sellToken +'", buyToken: "' + buyToken +'" }) { price, timestamp, id owner { id goodReputation badReputation } sellToken { id tokenSymbol } sellAmount buyAmount buyToken { id tokenSymbol } isPartial isBuyFiat auditor description isOpen } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

export async function getCommodityOffersByTokens(
    sellToken: string, 
    buyToken: string, 
    isOpen: boolean,
    payMethods: string[],
    country: string[],
    orderDirection: "asc" | "desc",
    orderBy: "timestamp" | "price" | "price_per_brute_weight" | "price_per_fine_weight",
    first: number,
    skip: number
) {
    let query = '{ offerCommodities(orderBy: '+ orderBy +', orderDirection: '+ orderDirection +', first: '+ first +', skip: '+ skip +', where: { payMethod_contains: ' + payMethods + ', country_contains: ' + country +', isOpen: '+ isOpen +', sellToken: "' + sellToken + '", buyToken: "' + buyToken + '" }) { price, price_per_brute_weight, price_per_fine_weight, timestamp, id owner { id goodReputation badReputation } sellId{ id gold { id reference metadata } } sellToken { id tokenSymbol } buyToken { id tokenSymbol } buyAmount description isOpen country subcountry subsubcountry payMethod } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

export async function getGoldOffersByTokensByJson(
    lawLow: number,
    lawHigh: number,
    weightBruteLow: number,
    weightBruteHigh: number,
    weightFineLow: number,
    weightFineHigh: number
) {
    let query = '{ golds(where: { law_gte: '+ lawLow +', law_lte: '+ lawHigh +', weight_brute_gte: '+ weightBruteLow +', weight_brute_lte: '+ weightBruteHigh +', weight_fine_gte: '+ weightFineLow +', weight_fine_lte: '+ weightFineHigh +', isLive: true, isP2P: true }) { id reference weight_fine weight_brute law commodity { offer { timestamp, price, price_per_brute_weight, price_per_fine_weight, id owner { id goodReputation badReputation } sellToken { id tokenSymbol } buyAmount buyToken { id tokenSymbol } description isOpen country subcountry subsubcountry payMethod } } } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

export async function getCommodityOffersByTokensByOwner(
    owner: string,
    sellToken: string, 
    buyToken: string, 
    isOpen: boolean,
    orderDirection: "asc" | "desc",
    orderBy: "timestamp" | "price" | "price_per_brute_weight" | "price_per_fine_weight",
    first: number,
    skip: number
) {
    let query = '{ offerCommodities(orderBy: '+ orderBy +', orderDirection: '+ orderDirection +', first: '+ first +', skip: '+ skip +', where: { owner: "' + owner + '", isOpen: '+ isOpen +', sellToken: "' + sellToken + '", buyToken: "' + buyToken + '" }) { price, price_per_brute_weight, price_per_fine_weight, timestamp, id owner { id goodReputation badReputation } sellId{ id gold { id reference metadata } } sellToken { id tokenSymbol } buyToken { id tokenSymbol } buyAmount description isOpen } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

export async function getDealsByUser(
    user: string,
    isPending: boolean
) {
    let query = '{ user(id: "'+ user +'") { id deals(where: { isPending: '+ isPending +' }) { id offer { timestamp, price, id sellToken { id tokenSymbol } buyToken { id tokenSymbol } auditor } seller { id goodReputation badReputation } buyer { id goodReputation badReputation } sellAmount buyAmount buyerVote sellerVote isPending isSuccess executor } } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

export async function getCommodityDealsByUser(
    user: string
) {
    let query = '{ user(id: "'+ user +'") { id commodityDeals { id buyer { id } } } }';
    let response = await querySubgraph(query, constants.P2P_SUBGRAPH);
    return response;
}

