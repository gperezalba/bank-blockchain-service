import { exchange, stringToBN } from "./blockchain.service";
import { Wallet } from "ethers";
import { ExchangeRequest } from "../classes/exchangeRequest";
import { TransactionReceipt } from "ethers/providers";


export async function exchangePi(
    signer: Wallet, 
    identity: string, 
    wallet: string,
    sendingToken: string,
    receivingToken: string,
    amount: string,
    kind: string
) 
    : Promise<TransactionReceipt>
{
    let exchangeRequest = new ExchangeRequest(sendingToken, receivingToken, stringToBN(amount), stringToBN(kind));
    return await exchange(signer, identity, wallet, exchangeRequest);
}

export async function exchangeFiat(
    signer: Wallet,
    identity: string,
    wallet: string,
    sendingToken: string,
    receivingToken: string,
    amount: string,
    kind: string
) {
    let exchangeRequest = new ExchangeRequest(sendingToken, receivingToken, stringToBN(amount), stringToBN(kind));
    //ToDo
    //return await exchangeDoble(signer, identity, wallet, exchangeRequest);
    //o implementarlo a nivel de SC si detecta que ambos tokens son NO Pi realiza doble exchange
}