import { transfer, transferSending, transferDomain, transferDomainSending, stringToBN, createWalletFromEncryptedJson, getContractCaller } from './blockchain.service';
import { constants } from '../environment/environment';
import { recoverPassword } from './recover-password.service';
import { Wallet } from 'ethers';
import { getIdentityByWallet, nicknameToWallet } from './query.service';

/******************** LOGIN */
/*
Este servicio se usará para 3 casos:
1. (login) Si la APP tiene toda la info de la identidad digital en el almacenamiento:
    a. Solo tendrá que desencriptar el wallet para poder firmar txs
    b. Una llamada a "login" le devuelve el objeto ethers.Wallet que usará después para firmar txs
2. (firstLogin) Si la APP solo tiene guardado el nickName y el encryptedWallet
    a. Esta situación se dará la primera vez que el usuario entra a la APP después de que se apruebe su registro
    b. La APP tendrá que pedir la info de su identidad digital(identity & wallet) para almacenarla
3. (emptyLogin) Si la APP no tiene nada por un cambio de móvil, borrado de datos de la APP en el dispositivo...:
    a. Se le pedirá al usuario su nickName y el mnemónico
    b. Se seguirá el mismo proceso que al recuperar contraseña
    c. Después el mismo proceso que en el punto 1.
*/
/************************** */

export async function login(password: string, encryptedWallet: string): Promise<Wallet> {
    return await createWalletFromEncryptedJson(encryptedWallet, password);
}

export async function firstLogin(nickName: string, encryptedWallet: string, password: string): Promise<any[]> {
    let signer = login(password, encryptedWallet);
    let digitalIdentity = await getDigitalIdentity(nickName);

    return [signer, digitalIdentity[0], digitalIdentity[1]];
}

export async function emptyLogin(mnemonic: string, password: string, nickName: string) {
    let signer = recoverPassword(mnemonic, password);
    let digitalIdentity = await getDigitalIdentity(nickName);

    return [signer, digitalIdentity[0], digitalIdentity[1]];
}

export async function getSigner(password: string, encryptedWallet: string): Promise<Wallet> {
    return await createWalletFromEncryptedJson(encryptedWallet, password);
}

export async function getDigitalIdentity(nickName: string): Promise<any[]> {
    let wallet = await nicknameToWallet(nickName);
    let identity = await getIdentityByWallet(wallet);

    return [identity, wallet];
}