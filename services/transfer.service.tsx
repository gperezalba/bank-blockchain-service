import { transfer, transferSending, transferDomain, transferDomainSending, stringToBN, transferNFT, transferNFTDomain, transferNFTRef, transferNFTRefDomain, transferPNFT, transferPNFTDomain } from './blockchain.service';
import { TransferRequest } from '../classes/transferRequest';
import { TransactionReceipt } from 'ethers/providers';
import { Wallet } from 'ethers';
import { TransferNFTRequest } from '../classes/transferNFTRequest';

/******************* TRANSFERIR */
/*
1. "transferReceive": El usuario fija la cantidad que quiere reciba el 
    destinatario (se le cobrará a parte la comisión).
2. "transferSending": El usuario fija la cantidad que quiere gastar en total
    (cantidad recibida en destino + comisión)
3. "transferReceiveDomain": ÍDEM 1 pero indicando nickname en lugar de address como desintatario
4. "transferSendDomain": ÍDEM 2 pero indicando nickname en lugar de address como desintatario

To Do:
- Faltan las funciones tipo CALL que digan p.ej cuánto será la comissión en cuanto indique la cantidad.

*/
/****************************** */

export async function transferReceive(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    to: string,
    amount: string,
    data: string, //concept of transfer,
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, to, stringToBN(amount), data, stringToBN(kind));
    return await transfer(signer, identity, wallet, transferRequest);
}

export async function transferSend(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    to: string,
    amount: string,
    data: string, //concept of transfer
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, to, stringToBN(amount), data, stringToBN(kind));
    return await transferSending(signer, identity, wallet, transferRequest);
}

export async function transferReceiveDomain(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    name: string,
    amount: string,
    data: string, //concept of transfer
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, name, stringToBN(amount), data, stringToBN(kind));
    return await transferDomain(signer, identity, wallet, transferRequest);
}

export async function transferSendDomain(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    name: string,
    amount: string,
    data: string, //concept of transfer
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, name, stringToBN(amount), data, stringToBN(kind));
    return await transferDomainSending(signer, identity, wallet, transferRequest);
}

export async function transferCommodity(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    to: string,
    id: string,
    data: string, //concept of transfer,
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, to, stringToBN(id), data, stringToBN(kind));
    return await transferNFT(signer, identity, wallet, transferRequest);
}

export async function transferCommodityDomain(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    name: string,
    id: string,
    data: string, //concept of transfer,
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, name, stringToBN(id), data, stringToBN(kind));
    return await transferNFTDomain(signer, identity, wallet, transferRequest);
}

export async function transferCommodityRef(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    to: string,
    reference: string,
    data: string, //concept of transfer,
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferNFTRequest(tokenAddress, to, reference, data, stringToBN(kind));
    return await transferNFTRef(signer, identity, wallet, transferRequest);
}

export async function transferCommodityDomainRef(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    name: string,
    reference: string,
    data: string, //concept of transfer,
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferNFTRequest(tokenAddress, name, reference, data, stringToBN(kind));
    return await transferNFTRefDomain(signer, identity, wallet, transferRequest);
}

export async function transferPackable(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    to: string,
    packableId: string,
    amount: string,
    data: string, //concept of transfer,
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, to, stringToBN(amount), data, stringToBN(kind), packableId);
    return await transferPNFT(signer, identity, wallet, transferRequest);
}

export async function transferPackableDomain(
    identity: string,
    wallet: string,
    signer: Wallet, 
    tokenAddress: string,
    name: string,
    packableId: string,
    amount: string,
    data: string, //concept of transfer,
    kind: string
)
    : Promise<TransactionReceipt> 
{
    let transferRequest = new TransferRequest(tokenAddress, name, stringToBN(amount), data, stringToBN(kind), packableId);
    return await transferPNFTDomain(signer, identity, wallet, transferRequest);
}