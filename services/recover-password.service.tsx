import { createWalletFromPrivKey, createWalletFromMnemonic, encryptWallet } from './blockchain.service';
import { constants } from '../environment/environment';

/*************RECUPERAR CONTRASEÑA */
/*
1. Si el usuario olvida su contraseña la puede recuperar haciendo uso del mnemónico
2. Basicamente consiste en eliminar el anterior encryptedWallet (ya que no tiene la contraseña vieja)
    y generar otro que encripte el mismo wallet pero con la contraseña nueva.
3. Se pasa el mnemónico y la nueva contraseña y se devuelve el nuevo encryptedWallet.
*/
/********************************* */

export async function recoverPassword(mnemonic: string, newPassword: string): Promise<string> {
    let owner = createWalletFromMnemonic(mnemonic, constants.PATH_0);
    let onlyOwner = createWalletFromPrivKey(owner.privateKey);
    let encryptedOwner = await encryptWallet(newPassword, onlyOwner);

    return encryptedOwner;
}