import { limitValue, stringToBN, createWalletFromMnemonic, limitTo, unlimitTo, unlimitValue, setState } from "./blockchain.service";
import { TransactionReceipt } from "ethers/providers";
import { constants } from "../environment/environment";

/**************** AJUSTES USUARIO (OPCIONAL DE MOMENTO) */
/*
El usuario puede:
1. Limitar el monto máximo de las transferencias que salgan de su wallet (discriminado por tokens)
2. Limitar los destinos a los que se pueden dirigir sus transferencias
3. Eliminar ambos limites
4. Desactivar su identidad digital (no podrá hacer nada hasta que no la vuelva a activar)
NOTA: PARA TODAS ESTAS ACCIONES SE NECESITA EL MNEMÓNICO
*/
/******************************** */


export async function walletLimitValue(
    mnemonic: string, 
    identity: string, 
    wallet: string, 
    tokenAddress: string, 
    limit: string
) 
    : Promise<TransactionReceipt>
{
    let recovery = createWalletFromMnemonic(mnemonic, constants.PATH_1);
    return await limitValue(recovery, identity, wallet, tokenAddress, stringToBN(limit));
}

export async function walletLimitTo(
    mnemonic: string, 
    identity: string, 
    wallet: string, 
    allowedReceiver: string
) 
    : Promise<TransactionReceipt>
{
    let recovery = createWalletFromMnemonic(mnemonic, constants.PATH_1);
    return await limitTo(recovery, identity, wallet, allowedReceiver);
}

export async function walletUnlimitValue(
    mnemonic: string,
    identity: string,
    wallet: string
) {
    let recovery = createWalletFromMnemonic(mnemonic, constants.PATH_1);
    return await unlimitValue(recovery, identity, wallet);
}

export async function walletUnlimitTo(
    mnemonic: string,
    identity: string,
    wallet: string
)
    : Promise<TransactionReceipt>
{
    let recovery = createWalletFromMnemonic(mnemonic, constants.PATH_1);
    return await unlimitTo(recovery, identity, wallet);
}

export async function setIdentityState(
    mnemonic: string, 
    identity: string, 
    newState: string //"0": Activo..."1": Inactivo
)
    : Promise<TransactionReceipt> 
{
    let recovery = createWalletFromMnemonic(mnemonic, constants.PATH_1);
    return await setState(recovery, identity, stringToBN(newState));
}