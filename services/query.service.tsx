import { querySubgraph } from "./blockchain.service";
import { constants } from "../environment/environment";

/********************** UTILS */

export async function walletToNickname(address: string) {
    let query = '{ wallet(id: "' + address + '"){ name { id } } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.wallet.name.id;
}

export async function nicknameToWallet(nickname: string) {
    let query = '{ wallets(where: { name: "' + nickname + '" }){ id } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.wallets[0].id;
}

export async function isNameAvailable(nickname: string) {
    let query = '{ wallets(where: { name: "' + nickname + '" }){ id } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.wallets.length == 0;
}

export async function isDataHashAvailable(dataHash: string) {
    let query = '{ identities(where: { dataHash: "' + dataHash + '" }){ id } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.identities.length == 0;
}

/********************** IDENTITY */

export async function getIdentity(
    identity: string
) {
    let query = '{ identities(id: "' + identity + '"){ id dataHash owner recovery wallet { id name { id } } creationTime lastModification } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.identities[0].id;
}

export async function getIdentityByName(
    nickname: string
) {
    let address = await nicknameToWallet(nickname);
    let response = await getIdentityByWallet(address);
    return response;
}

export async function getIdentityByWallet(
    address: string
) {
    let query = '{ identities(where: { wallet: "' + address + '" }){ id dataHash owner recovery wallet { id name { id } } creationTime lastModification } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.identities[0].id;
}

export async function getIdentityByOwner(
    owner: string
) {
    let query = '{ identities(where: { owner: "' + owner + '" }){ id dataHash owner recovery wallet { id name { id } } creationTime lastModification } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.identities[0].id;
}

export async function getIdentityByDataHash(
    dataHash: string
) {
    let query = '{ identities(where: { dataHash: "' + dataHash + '" }){ id dataHash owner recovery wallet { id name { id } } creationTime lastModification } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response.identities[0].id;
}

/********************** WALLET TRANSACTIONS */

export async function getTransactionDetails(txId: string) {
    let query = '{ transaction(id: "' + txId + '") { from { id name { id } } to { id name { id } } amount currency { tokenSymbol } fee bankTransaction { kind concept bankFee { fee } } timestamp } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response;
}

export async function getWalletBalancesAndTransactionsByWallet(
    address: string,
    first: number,
    skip: number
)
    : Promise<any>
{
    let query = '{ wallets(where: { id: "' + address + '" }) { transactions(orderBy: timestamp, orderDirection: desc, first: ' + first + ', skip: ' + skip + '){ id from { id name { id } } to { id name { id } } amount bankTransaction { concept kind } } balances { balance token { tokenSymbol } } } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response;
}

export async function getWalletBalancesAndTransactionsByName(
    nickname: string,
    first: number,
    skip: number
)
    : Promise<any>
{
    let query = '{ wallets(where: { name: "' + nickname + '" }) { transactions(orderBy: timestamp, orderDirection: desc, first: ' + first + ', skip: ' + skip + '){ id from { id name { id } } to { id name { id } } amount bankTransaction { concept kind } } balances { balance token { tokenSymbol } } } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response;
}

export async function getTransactionsFromWallet(
    address: string,
    first: number,
    skip: number
) {
    let query = '{ transactions( orderBy: timestamp, orderDirection: desc, first: ' + first + ', skip: ' + skip + ', where: { from: "' + address + '" } ) { id timestamp from { id name { id } } to { id name { id } } amount currency { tokenSymbol } bankTransaction { concept } } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response;
}

export async function getTransactionsToWallet(
    address: string,
    first: number,
    skip: number
) {
    let query = '{ transactions( orderBy: timestamp, orderDirection: desc, first: ' + first + ', skip: ' + skip + ', where: { to: "' + address + '" } ) { id timestamp from { id name { id } } to { id name { id } } amount currency { tokenSymbol } bankTransaction { concept } } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response;
}

export async function getTransactionsFromWalletByToken(
    address: string,
    first: number,
    skip: number,
    tokenAddress: string
) {
    let query = '{ transactions( orderBy: timestamp, orderDirection: desc, first: ' + first + ', skip: ' + skip + ', where: { from: "' + address + '", currency: "' + tokenAddress + '" } ) { id timestamp from { id name { id } } to { id name { id } } amount currency { tokenSymbol } bankTransaction { concept } } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response;
}

export async function getTransactionsToWalletByToken(
    address: string,
    first: number,
    skip: number,
    tokenAddress: string
) {
    let query = '{ transactions( orderBy: timestamp, orderDirection: desc, first: ' + first + ', skip: ' + skip + ', where: { to: "' + address + '", currency: "' + tokenAddress + '" } ) { id timestamp from { id name { id } } to { id name { id } } amount currency { tokenSymbol } bankTransaction { concept } } }';
    let response = await querySubgraph(query, constants.BANK_SUBGRAPH);
    return response;
}

/********************** MARKETS */

export async function getMarket(
    currency1: string,
    currency2: string
) {
    let query = '{ markets(where: { currency1: "' + currency1 + '", currency2: "' + currency2 + '" }){ id change commission currency1 { tokenSymbol } currency2 { tokenSymbol } currency1Balance currency2Balance updated } }';
    let response = await querySubgraph(query, constants.MARKETS_SUBGRAPH);
    return response;
}

export async function getAllMarkets() {
    let query = '{ markets { id change commission currency1 { tokenSymbol } currency2 { tokenSymbol } currency1Balance currency2Balance updated } }';
    let response = await querySubgraph(query, constants.MARKETS_SUBGRAPH);
    return response;
}