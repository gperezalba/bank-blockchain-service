import { ethers, Contract } from 'ethers';
import { constants } from '../environment/environment';
import { TransferRequest } from '../classes/transferRequest';
import { ExchangeRequest } from '../classes/exchangeRequest';
import { TransferNFTRequest } from '../classes/transferNFTRequest';

function getProvider(): ethers.providers.BaseProvider {
    return new ethers.providers.JsonRpcProvider(constants.URL);
}

export function createWalletFromEntropy(entropy: string): ethers.Wallet {
    return ethers.Wallet.createRandom(entropy);
}

//let mnemonic = "shadow gain island same rely plate either isolate picture museum shield surface";
export function createWalletFromMnemonic(mnemonic: string, path?: string): ethers.Wallet {
    //requerir isValidMnemonic == true?
    return ethers.Wallet.fromMnemonic(mnemonic, path);
}

export function isValidMnemonic(mnemonic: string): boolean {
    return ethers.utils.HDNode.isValidMnemonic(mnemonic);
}

export function isAddress(address: string): boolean {
    try {
        ethers.utils.getAddress(address);
    } catch (e) { return false; }
    return true;
}

//let privateKey = "0x0123456789012345678901234567890123456789012345678901234567890123";
export function createWalletFromPrivKey(privateKey: string): ethers.Wallet {
    return new ethers.Wallet(privateKey);
}

//let encryptedJson = {"version":3,"id":"148ea69d-1f23-48dc-a7d8-c316be23d0d3","address":"19e8d14be53249cd4aa1c8cb43425c0aeeb67fff","crypto":{"ciphertext":"8dc58c56ee705cfc661951397828bb2d464f1db9cad833cd1a6ef09eca7bb177","cipherparams":{"iv":"c6353dc63ef390991d7940e4efe0a8a7"},"cipher":"aes-128-ctr","kdf":"scrypt","kdfparams":{"dklen":32,"salt":"5a36acf0cc140298403787d1e1f957429ecd5a861064186df3f1a143c63dc651","n":131072,"r":8,"p":1},"mac":"3fcfc949627f018766a137e9235c984fa38997b4619aff8172a892c648373917"}};
//let pass = "contraprueba";
export async function createWalletFromEncryptedJson(encryptedJson: string, pasword: string): Promise<ethers.Wallet> {
    return await ethers.Wallet.fromEncryptedJson(encryptedJson, pasword);
}

export async function encryptWallet(password: string, wallet: ethers.Wallet): Promise<string> {
    return await wallet.encrypt(password);
}

export async function sendTransaction(wallet: ethers.Wallet, transaction: any): Promise<ethers.providers.TransactionReceipt> {
    let populatedTransaction = await createTransaction(wallet, transaction);
    let signedTransaction = await signTransaction(wallet, populatedTransaction);
    let response = await sendSignedTransaction(signedTransaction);
    let receipt = await response.wait();

    return receipt;
}

export async function createTransaction(wallet: ethers.Wallet, transaction: any): Promise<ethers.providers.TransactionRequest> {
    const customHttpProvider = getProvider();
    let populatedTransaction = await ethers.utils.populateTransaction(transaction, customHttpProvider, wallet.address);

    if (populatedTransaction.gasPrice.lt(ethers.utils.bigNumberify(constants.MIN_GAS_PRICE))) {
        populatedTransaction.gasPrice = ethers.utils.bigNumberify(constants.MIN_GAS_PRICE);
    }

    return populatedTransaction;
}

export async function signTransaction(wallet: ethers.Wallet, transaction: ethers.providers.TransactionRequest): Promise<string> {
    let walletInstance = new ethers.Wallet(wallet.privateKey);
    return await walletInstance.sign(transaction);
}

export async function sendSignedTransaction(signedTransaction: string): Promise<ethers.providers.TransactionResponse> {
    const customHttpProvider = getProvider();
    return await customHttpProvider.sendTransaction(signedTransaction);
}

export function etherStringToWeiBN(ether: string): ethers.utils.BigNumber {
    return ethers.utils.parseEther(ether);
}

export function etherStringToWeiString(ether: string): string {
    return ethers.utils.parseEther(ether).toString();
}

export function weiStringToEtherBN(wei: string): ethers.utils.BigNumber {
    return ethers.utils.bigNumberify(ethers.utils.formatEther(wei));
}

export function weiToEtherBN(weiBN: ethers.utils.BigNumber): ethers.utils.BigNumber {
    return ethers.utils.bigNumberify(ethers.utils.formatEther(weiBN));
}

export function weiToEther(wei: string): string {
    return ethers.utils.formatEther(wei);
}

export function weiBNToEtherString(bn: ethers.utils.BigNumber): string {
    return ethers.utils.formatEther(bn.toString());
}

export function BNToString(bn: ethers.utils.BigNumber): string {
    return bn.toString();
}

export function stringToBN(str: string): ethers.utils.BigNumber {
    return ethers.utils.bigNumberify(str);
}

export function commify(ether: string): string {
    return ethers.utils.commify(ether);
}

export function commifyBN(bn: ethers.utils.BigNumber): string {
    return ethers.utils.commify(bn.toString());
}

export function getContractCaller(address: string, abi: any | ethers.utils.Interface): ethers.Contract {
    return new ethers.Contract(address, abi, getProvider());
}

export function getContractSigner(address: string, abi: any | ethers.utils.Interface, wallet: ethers.Wallet): ethers.Contract {
    //requerir wallet.provider != null?
	wallet = wallet.connect(getProvider());
    return getContractCaller(address, abi).connect(wallet);
}

export async function getPastEvents(contract: ethers.Contract, filter: ethers.EventFilter, fromBlock: number, toBlock: number | string) {
    let provider = getProvider();
    let logsFilter = {
        address: contract.address,
        fromBlock: fromBlock,
        toBlock: toBlock,
        topics: filter.topics
    }
    let logs = await provider.getLogs(logsFilter);

    return logs;
}

export async function decodeEvent(contract: ethers.Contract, log: {topics: string [], data: string}): Promise<any> {
    return await contract.interface.parseLog(log).values;
}

export async function querySubgraph(query: string, subgraph: string): Promise<any> {
    let response: any;
    let responseData: any;

    try {
        response = await fetch(constants.GRAPH_URL + subgraph, {
            "method": 'POST',
            "headers": {
                "Accept": 'application/json',
                'Content-Type': 'application/json',
            },
            "body": JSON.stringify({
                query: query
            }),
        });
    } catch (error) {
        console.error(error);
    }

    if (response.ok) {
        responseData = await response.json();
    }
    
    return responseData.data;
}

/*
QUERY RULES

1. With ID
{
  token(id: "1") {
    id
    owner
  }
}

2. Without ID
{
  tokens {
    id
    owner
  }
}

3. Sorting
{
  tokens(orderBy: price, orderDirection: asc | desc) {
    id
    owner
  }
}

4. Pagination
{
  tokens(first:1000, skip:1000) { //[1000-1999]
    id
    owner
  }
}

5. Filtering
{
  challenges(where: { outcome: "failed" }) {
    challenger
  }
}

6. Filtering with suffixes
{
  applications(where: { deposit_gt: "10000000000" }) {
    id
    deposit
  }
}

_not
_gt
_lt
_gte
_lte
_in
_not_in
_contains
_not_contains
_starts_with
_ends_with
_not_starts_with
_not_ends_with

7. Query a certain past block
{
  challenges(block: { number: 8000000 }) {
    challenger
    outcome
  }
}

{
  challenges(block: { hash: "0x5a0b54d5dc17e0aadc383d2db43b0a0d3e029c4c" }) {
    challenger
    outcome
  }
}

*/

/*

BANK QUERIES

1. SALDOS EN TODOS LOS TOKENS QUE POSEE EL WALLET
{
    wallet (where: {id: "wallet_address"}) {
        balances {
            token {
                tokenSymbol
            }
            balance
        }
    }
}

2. ÚLTIMOS 20 MOVIMIENTOS DEL WALLET
{
    transactions 
    (
        where: {currency: "token_address", from: "wallet_address"},
        first: 20,
        skip: 20,
        orderBy: timestamp,
        orderDirection: desc
    ) 
    {
        from
        to
        amount
        timestamp
        bankTransaction {
            concept
        }
    }
}

*/

/**********************CONTROLLER FUNCTIONS */

export async function getControllerAddress(index: string): Promise<any> {
    //ToDo: cambiar por una query al subgraph
    let controllerContract = getContractCaller(constants.CONTROLLER_ADDRESS, constants.CONTROLLER_ABI);
    return await controllerContract.addresses(index);
}

/**********************IDENTITY FUNCTIONS */

export async function forward(signer: ethers.Wallet, identity: string, destination: string, data: string): Promise<ethers.providers.TransactionReceipt>  {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.forward(destination, data, constants.OVERRIDES);
    return await response.wait();
}

export async function setState(signer: ethers.Wallet, identity: string, newState: ethers.utils.BigNumber): Promise<ethers.providers.TransactionReceipt> {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.setState(newState);
    return await response.wait();
}    

export async function setOwner(signer: ethers.Wallet, identity: string, newOwner: ethers.utils.BigNumber): Promise<ethers.providers.TransactionReceipt> {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.setOwner(newOwner);
    return await response.wait();
}    

export async function setRecovery(signer: ethers.Wallet, identity: string, newRecovery: ethers.utils.BigNumber): Promise<ethers.providers.TransactionReceipt> {
    let identityContract = getContractSigner(identity, constants.IDENTITY_ABI, signer);
    let response = await identityContract.setRecovery(newRecovery);
    return await response.wait();
}    

/**********************WALLET FUNCTIONS */

export async function transfer(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    //let wallet = getWalletByIdentity(identity); DEBO TENER MAPEO DE ESTO AQUÍ? O DONDE?
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transfer.encode([tx.tokenAddress, tx.destination, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferSending(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferSending.encode([tx.tokenAddress, tx.destination, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferDomain(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferDomain.encode([tx.tokenAddress, tx.destination, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeReceiving(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, transferingAmount: ethers.utils.BigNumber, to: string, data: string}): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeReceiving.encode([tx.sendingToken, tx.transferingToken, tx.transferingAmount, tx.to, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeSending(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, sendingAmount: ethers.utils.BigNumber, to: string, data: string}): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeSending.encode([tx.sendingToken, tx.transferingToken, tx.sendingAmount, tx.to, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferDomainSending(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferDomainSending.encode([tx.tokenAddress, tx.destination, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeDomainReceiving(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, sendingAmount: ethers.utils.BigNumber, name: string, data: string}): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeDomainReceiving.encode([tx.sendingToken, tx.transferingToken, tx.sendingAmount, tx.name, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function transferExchangeDomainSending(signer: ethers.Wallet, identity: string, wallet: string, tx: {sendingToken: string, transferingToken: string, sendingAmount: ethers.utils.BigNumber, name: string, data: string}): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferExchangeDomainSending.encode([tx.sendingToken, tx.transferingToken, tx.sendingAmount, tx.name, tx.data]);
    return await forward(signer, identity, wallet, data);
}

export async function exchange(signer: ethers.Wallet, identity: string, wallet: string, tx: ExchangeRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.exchange.encode([tx.sendingToken, tx.receivingToken, tx.amount, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function limitValue(signer: ethers.Wallet, identity: string, wallet: string, tokenAddress: string, limit: ethers.utils.BigNumber): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.limitValue.encode([tokenAddress, limit]);
    return await forward(signer, identity, wallet, data);
}

export async function limitTo(signer: ethers.Wallet, identity: string, wallet: string, receiver: string): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.limitTo.encode([receiver]);
    return await forward(signer, identity, wallet, data);
}

export async function unlimitValue(signer: ethers.Wallet, identity: string, wallet: string): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.unlimitValue.encode([]);
    return await forward(signer, identity, wallet, data);
}

export async function unlimitTo(signer: ethers.Wallet, identity: string, wallet: string): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.unlimitValue.encode([]);
    return await forward(signer, identity, wallet, data);
}

export async function transferNFT(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferNFT.encode([tx.tokenAddress, tx.destination, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferNFTDomain(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferNFTDomain.encode([tx.tokenAddress, tx.destination, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferNFTRef(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferNFTRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferNFTRef.encode([tx.tokenAddress, tx.destination, tx.reference, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferNFTRefDomain(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferNFTRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferNFTRefDomain.encode([tx.tokenAddress, tx.destination, tx.reference, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferPNFT(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferPNFT.encode([tx.tokenAddress, tx.destination, tx.packableId, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

export async function transferPNFTDomain(signer: ethers.Wallet, identity: string, wallet: string, tx: TransferRequest): Promise<ethers.providers.TransactionReceipt> {
    let walletContract = getContractSigner(wallet, constants.WALLET_ABI, signer);
    let data = walletContract.interface.functions.transferPNFTDomain.encode([tx.tokenAddress, tx.destination, tx.packableId, tx.amount, tx.data, tx.kind]);
    return await forward(signer, identity, wallet, data);
}

//CALLS

export async function getTransferExchangeInfoSending(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getTransferExchangeInfoSending();
    return response;
}

export async function getTransferExchangeInfoReceiving(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getTransferExchangeInfoReceiving();
    return response;
}

export async function getExchangeInfoSending(wallet: string): Promise<[ethers.utils.BigNumber]> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getExchangeInfoSending();
    return response;
}

export async function getExchangeInfoReceiving(wallet: string): Promise<[ethers.utils.BigNumber]> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getExchangeInfoReceiving();
    return response;
}

export async function getValueToSpend(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getValueToSpend();
    return response;
}

export async function getSpendToValue(wallet: string): Promise<ethers.utils.BigNumber> {
    let walletContract = getContractCaller(wallet, constants.WALLET_ABI);
    let response = await walletContract.getSpendToValue();
    return response;
}


