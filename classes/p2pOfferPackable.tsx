import { ethers } from "ethers";

export class P2POfferPackable {

    tokens: string[] = [];
    amounts: ethers.utils.BigNumber[] = [];
    tokenId: string;
    settings: boolean[] = [];
    limits: ethers.utils.BigNumber[] = [];
    auditor: string;
    description: string;
    metadata: number[] = [];

    constructor(
        sellToken: string,
        sellAmount: ethers.utils.BigNumber,
        tokenId: string,
        isPartial: boolean,
        buyToken: string,
        buyAmount: ethers.utils.BigNumber,
        isBuyFiat: boolean,
        minDealAmount: ethers.utils.BigNumber,
        maxDealAmount: ethers.utils.BigNumber,
        minReputation: ethers.utils.BigNumber,
        auditor: string,
        description: string,
        country: number[],
        payMethods: number[]
    ) {
        this.tokens.push(sellToken);
        this.tokens.push(buyToken);
        this.amounts.push(sellAmount);
        this.amounts.push(buyAmount);
        this.tokenId = tokenId;
        this.settings.push(isPartial);
        this.settings.push(isBuyFiat);
        this.limits.push(minDealAmount);
        this.limits.push(maxDealAmount);
        this.limits.push(minReputation);
        this.auditor = auditor;
        this.description = description;

        let zero = [0];
        let metadata = country.concat(zero).concat(payMethods).concat(zero);
        this.metadata = metadata;
    }
}