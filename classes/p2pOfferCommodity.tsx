import { ethers } from "ethers";

export class P2POfferCommodity {

    sellToken: string;
    sellId: ethers.utils.BigNumber;
    buyToken: string;
    buyAmount: ethers.utils.BigNumber;
    isBuyFiat: boolean;
    minReputation: ethers.utils.BigNumber;
    auditor: string;
    description: string;
    metadata: number[] = [];

    constructor(
        sellToken: string,
        sellId: ethers.utils.BigNumber,
        buyToken: string,
        buyAmount: ethers.utils.BigNumber,
        isBuyFiat: boolean,
        minReputation: ethers.utils.BigNumber,
        auditor: string,
        description: string,
        country: number[],
        payMethods: number[]
    ) {
        this.sellToken = sellToken;
        this.sellId = sellId;
        this.buyToken = buyToken;
        this.buyAmount = buyAmount;
        this.isBuyFiat = isBuyFiat;
        this.minReputation = minReputation;
        this.auditor = auditor;
        this.description = description;

        let zero = [0];
        let metadata = country.concat(zero).concat(payMethods).concat(zero);
        this.metadata = metadata;
    }
}