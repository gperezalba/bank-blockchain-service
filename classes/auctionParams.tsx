import { ethers } from "ethers";

export class AuctionParams {

    readonly auditor: string;
    readonly tokens: string[];
    readonly auctionAmountOrId: ethers.utils.BigNumber;
    readonly settings: ethers.utils.BigNumber[];
    readonly auctionTokenId: string;

    constructor(
        auditor: string,
        auctionToken: string,
        bidToken: string,
        auctionAmountOrId: ethers.utils.BigNumber,
        minValue: ethers.utils.BigNumber,
        endTime: ethers.utils.BigNumber,
        auctionTokenId?: string
    ) {
        this.auditor = auditor;
        this.tokens = [];
        this.tokens.push(auctionToken);
        this.tokens.push(bidToken);
        this.auctionAmountOrId = auctionAmountOrId;
        this.settings = [];
        this.settings.push(minValue);
        this.settings.push(endTime);

        if (auctionTokenId == undefined) {
            this.auctionTokenId = ethers.constants.HashZero;
        } else {
            this.auctionTokenId = auctionTokenId;
        }
    }
}