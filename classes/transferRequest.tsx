import { ethers } from "ethers";

export class TransferRequest {

    tokenAddress: string;
    destination: string;
    amount: ethers.utils.BigNumber;
    data: string;
    kind: ethers.utils.BigNumber;
    packableId?: string;

    constructor(
        tokenAddress: string,
        destination: string, //address or nickname
        amount: ethers.utils.BigNumber,
        data: string,
        kind: ethers.utils.BigNumber,
        packableId?: string
    ) {
        this.tokenAddress = tokenAddress;
        this.destination = destination;
        this.amount = amount;
        this.data = data;
        this.kind = kind;
        
        if (packableId != undefined) {
            this.packableId = packableId;
        }
    }
}