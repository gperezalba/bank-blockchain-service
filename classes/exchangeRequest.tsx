import { ethers } from "ethers";

export class ExchangeRequest {

    sendingToken: string;
    receivingToken: string;
    amount: ethers.utils.BigNumber; //SendingAmount or ReceivingAmount depends on the case
    kind: ethers.utils.BigNumber;

    constructor(
        sendingToken: string,
        receivingToken: string,
        amount: ethers.utils.BigNumber,
        kind: ethers.utils.BigNumber
    ) {
        this.sendingToken = sendingToken;
        this.receivingToken = receivingToken;
        this.amount = amount;
        this.kind = kind;
    }
}