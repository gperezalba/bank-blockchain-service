export class WalletPair {

    owner: string;
    recovery: string;
    mnemonic: string;
    encryptedWallet: string;

    constructor(owner: string, recovery: string, mnemonic: string, encryptedWallet: string) {
        this.owner = owner;
        this.recovery = recovery;
        this.mnemonic = mnemonic;
        this.encryptedWallet = encryptedWallet;
    }
}