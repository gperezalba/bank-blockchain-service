export class WalletPairSafe {

    owner: string;
    recovery: string;
    mnemonic: string;
    encryptedWallet: string;
    encryptedWalletMnemonic: string;

    constructor(
        owner: string, 
        recovery: string, 
        mnemonic: string, 
        encryptedWallet: string,
        encryptedWalletMnemonic: string
    ) {
        this.owner = owner;
        this.recovery = recovery;
        this.mnemonic = mnemonic;
        this.encryptedWallet = encryptedWallet;
        this.encryptedWalletMnemonic = encryptedWalletMnemonic;
    }
}