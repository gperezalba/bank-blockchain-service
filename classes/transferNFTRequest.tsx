import { ethers } from "ethers";

export class TransferNFTRequest {

    tokenAddress: string;
    destination: string;
    reference: string;
    data: string;
    kind: ethers.utils.BigNumber;

    constructor(
        tokenAddress: string,
        destination: string, //address or nickname
        reference: string,
        data: string,
        kind: ethers.utils.BigNumber
    ) {
        this.tokenAddress = tokenAddress;
        this.destination = destination;
        this.reference = reference;
        this.data = data;
        this.kind = kind;
    }
}